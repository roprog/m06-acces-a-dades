import java.util.ArrayList;
import java.util.List;

public class Client {

    String nif;
    String nom;
    int total_facturacio;
    int telefon;
    String correu;
    List<Comanda> comandes = new ArrayList<Comanda>();

    public Client() {
    }

    public Client(String nif, String nom, int total_facturacio) {
        this.nif = nif;
        this.nom = nom;
        this.total_facturacio = total_facturacio;
    }

    public Client(String nif, String nom, int total_facturacio, int telefon, String correu) {
        this.nif = nif;
        this.nom = nom;
        this.total_facturacio = total_facturacio;
        this.telefon = telefon;
        this.correu = correu;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getTotal_facturacio() {
        return total_facturacio;
    }

    public void setTotal_facturacio(int total_facturacio) {
        this.total_facturacio = total_facturacio;
    }

    public int getTelefon() {
        return telefon;
    }

    public void setTelefon(int telefon) {
        this.telefon = telefon;
    }

    public String getCorreu() {
        return correu;
    }

    public void setCorreu(String correu) {
        this.correu = correu;
    }

    @Override
    public String toString() {
        return "Client{" + "nif=" + nif + ", nom=" + nom + ", total_facturacio=" + total_facturacio + ", telefon=" + telefon + ", correu=" + correu + ", comandes=" + comandes + '}';
    }

}

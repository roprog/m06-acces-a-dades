import java.util.Date;

public class Comanda {

    Date data_comanda;
    int importt;
    boolean pagada;

    public Comanda() {
    }

    public Comanda(Date data_comanda, int importt, boolean pagada) {
        this.data_comanda = data_comanda;
        this.importt = importt;
        this.pagada = pagada;
    }

    @Override
    public String toString() {
        return "Comanda {" + "data_comanda=" + data_comanda + ", importt=" + importt + ", pagada=" + pagada + '}';
    }

}

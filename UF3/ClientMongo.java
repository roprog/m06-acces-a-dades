import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import static com.mongodb.client.model.Filters.eq;

public class ClientMongo {

    public static void exercici1() {

        try(MongoClient mongoClient= MongoClients.create();) {
            MongoDatabase database= mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection= database.getCollection("clients");

            Document doc= new Document("nif", "12345678X")
                    .append("nom", "Pere Pons")
                    .append("total_facturacio", 0);
            collection.insertOne(doc);
        }
    }

    public static void exercici2(String nif, String nom, int total_facturacio, int telefon, String correu) {

        try(MongoClient mongoClient= MongoClients.create();) {
            MongoDatabase database= mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection= database.getCollection("clients");
            Document doc;

            if(telefon != 0 && correu != null) { // Si el client te telefon i correu
                doc= new Document("nif", nif)
                        .append("nom", nom)
                        .append("total_facturacio", total_facturacio)
                        .append("telefon", telefon)
                        .append("correu", correu);
            } else if (telefon != 0 && correu == null) { // Si el client te telefon pero no correu
                doc= new Document("nif", nif)
                        .append("nom", nom)
                        .append("total_facturacio", total_facturacio)
                        .append("telefon", telefon);
            } else if (telefon == 0 && correu != null) { // Si el client no te telefon pero si correu
                doc= new Document("nif", nif)
                        .append("nom", nom)
                        .append("total_facturacio", total_facturacio)
                        .append("correu", correu);
            } else { // Si el client no te telefon ni correu
                doc= new Document("nif", nif)
                        .append("nom", nom)
                        .append("total_facturacio", total_facturacio);
            }

            collection.insertOne(doc);

        }
    }

    public static void exercici3() {
        MongoClient mongoClient= MongoClients.create();
        MongoDatabase database= mongoClient.getDatabase("bd_prova");
        MongoCollection<Document> collection= database.getCollection("clients");

        try (MongoCursor<Document> cursor = collection.find().iterator()) {
            while(cursor.hasNext()) {
                Document d = cursor.next();
                System.out.println(d.getString("nif"));
            }
        }
    }

    public static void exercici4() throws ParseException {

        Scanner sc = new Scanner(System.in);
        int totalFacturacio;

        MongoClient mongoClient= MongoClients.create();
        MongoDatabase database= mongoClient.getDatabase("bd_prova");
        MongoCollection<Document> collection= database.getCollection("clients");

        int contador = 1;

        //Printem els nif de tots els clients
        try (MongoCursor<Document> cursor = collection.find().iterator()) {
            while(cursor.hasNext()) {
                Document d = cursor.next();
                System.out.println(contador + ") " + d.getString("nif"));
                contador++;
            }
        }

        //Demanem quin client vol, i esl valors de les comandes
        System.out.println("Selecciona el NIF del client, per afegir una comanda:");
        int opcio = sc.nextInt();

        System.out.println("Introdueix la data, de la comanda:");
        String dataNoFormet = sc.next();

        System.out.println("Introduexi l'import facturat");
        int importt = sc.nextInt();

        System.out.println("Ha estat pagada");
        boolean pagada = sc.nextBoolean();

        //Formategem el String dataNoFormet
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse(dataNoFormet);

        try (MongoCursor<Document> cursor = collection.find().iterator()) {
            while(cursor.hasNext()) {
                Document d = cursor.next();
                if (contador == opcio) {
                    //Recuperem el client, que ha demanat l'usuari i la llista de comandes que te
                    Client c = d.get("client", Client.class);
                    List<Comanda> llistaComandes = d.getList("comandes", Comanda.class);

                    //Sumem el total de facturacio que tenia el clients abans, amb l'import de la nova comanda
                    totalFacturacio = c.getTotal_facturacio();
                    totalFacturacio = totalFacturacio + importt;

                    //Creem la nova comanda i l'afegim a la llista de comandes del client
                    Comanda comanda = new Comanda(date, totalFacturacio, pagada);
                    llistaComandes.add(comanda);

                    //Eliminem el client de la BD, per poguer-lo actualitzar
                    DeleteResult deleteResult = collection.deleteMany(eq("nif", c.getNif()));

                    //Creem el nou client, amb las dades del que hem guardat abans
                    Document doc = new Document("nif", c.getNif())
                            .append("nom", c.getNom())
                            .append("total_facturacio", c.getTotal_facturacio())
                            .append("telefon", c.getTelefon())
                            .append("correu", c.getCorreu());

                    //Afegim la llista de comandes
                    List<Document> arrayComandes = new ArrayList<>();
                    for (Comanda comandaSelect : llistaComandes) {
                        arrayComandes.add(new Document("data_comanda", comandaSelect.data_comanda)
                                .append("importt", comandaSelect.importt)
                                .append("pagada", comandaSelect.pagada));
                    }
                    doc.append("comandes", arrayComandes);

                    //Inserim el client amb la nova comanda a la BD
                    collection.insertOne(doc);
                }
            }
        }
    }

    public static void exercici5(int facturacio) {

        MongoClient mongoClient= MongoClients.create();
        MongoDatabase database= mongoClient.getDatabase("bd_prova");
        MongoCollection<Document> collection= database.getCollection("clients");

        try (MongoCursor<Document> cursor = collection.find(gte("total_facturacio", facturacio)).iterator()) {
            while (cursor.hasNext()) {
                Document d = cursor.next();
                System.out.println("Nif del client: " + d.getString("nif"));
                System.out.println("Nom del client: " + d.getString("nom"));
                System.out.println("Total facturat pel client: " + d.getInteger("total_facturacio"));
                System.out.println("Telefon del client: " + d.getInteger("telefon"));
                System.out.println("Correu del client: " + d.getString("correu"));

                int contador = 1;
                List<Document> llistaComandes = d.getList("comandes", Document.class);
                for (Document subdoc : llistaComandes) {
                    System.out.println("Comanda " + contador);
                    contador++;
                    System.out.println("Data de la comanda: " + subdoc.getDate("data_comanda"));
                    System.out.println("Import de la comanda: " + subdoc.getInteger("importt"));
                    System.out.println("Esta pagada? " + subdoc.getBoolean("pagada"));
                    System.out.println("======================================");
                }
            }
        }
    }

    public static void exercici6(int cantComandes) {
        MongoClient mongoClient= MongoClients.create();
        MongoDatabase database= mongoClient.getDatabase("bd_prova");
        MongoCollection<Document> collection= database.getCollection("clients");

        try (MongoCursor<Document> cursor = collection.find(gte("comandes", cantComandes)).iterator()) {
            while (cursor.hasNext()) {
                Document d = cursor.next();
                System.out.println("Nif del client: " + d.getString("nif"));
                System.out.println("Nom del client: " + d.getString("nom"));
                System.out.println("Total facturat pel client: " + d.getInteger("total_facturacio"));
                System.out.println("Telefon del client: " + d.getInteger("telefon"));
                System.out.println("Correu del client: " + d.getString("correu"));

                int contador = 1;
                List<Document> llistaComandes = d.getList("comandes", Document.class);
                for (Document subdoc : llistaComandes) {
                    System.out.println("Comanda " + contador);
                    contador++;
                    System.out.println("Data de la comanda: " + subdoc.getDate("data_comanda"));
                    System.out.println("Import de la comanda: " + subdoc.getInteger("importt"));
                    System.out.println("Esta pagada? " + subdoc.getBoolean("pagada"));
                    System.out.println("======================================");
                }
            }
        }
    }
}

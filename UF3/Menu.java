import java.text.ParseException;
import java.util.Scanner;

public class Menu {

    public static int menu() {

        System.out.println("    Seleccioneu una opció:"
                + "\n ===================================="
                + "\n 0) Sortir"
                + "\n 1) Donar d'alta un client"
                + "\n 2) Afegir comanda a un client"
                + "\n 3) Cercar clients per facturació"
                + "\n 4) Cercar clients per quantitat de comandes");

        Scanner resposta = new Scanner(System.in);
        return resposta.nextInt();

    }

    public static void afegirClient() {

        Scanner sc = new Scanner(System.in);

        System.out.println("1) Donar d'alta un client: "
                + "\n _________________________________________________"
                + "\n A continuacio introdueix les dades del client que vols donar d'alta");
        System.out.println("Intrudueix el NIF:");
        String nif = sc.next();
        System.out.println("Intrudueix el nom:");
        String nom = sc.next();
        System.out.println("Intrudueix el total facturat:");
        int total_facturacio = sc.nextInt();
        System.out.println("Intrudueix el telefon, si no el vols afegir, premi la tecla 0 + Enter:");
        int telefon = sc.nextInt();
        System.out.println("Intrudueix el correu, si no el vols afegir, premi la tecla a + Enter:");
        String correu = sc.next();

        if ("a".equals(correu)) {
            ClientMongo.exercici2(nif, nom, total_facturacio, telefon, null);
        } else {
            ClientMongo.exercici2(nif, nom, total_facturacio, telefon, correu);
        }
    }

    private static void afegirComandaClient() throws ParseException {
        System.out.println("2) Afegir comanda a un client:");
        ClientMongo.exercici4();
    }

    public static void consultarClientFacturacio() {
        Scanner sc = new Scanner(System.in);

        System.out.println("3) Cercar clients per facturació:");
        System.out.print("Introdueix la factura minima que ha de tenir el client ");

        ClientMongo.exercici5(sc.nextInt());
    }

    public static void consultarClientComandes() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Cercar clients per quantitat de comandes:");
        System.out.print("Introdueix la quantitat minima de comandes que ha de tindre el client ");

        ClientMongo.exercici6(sc.nextInt());
    }

    public static void main(String[] args) throws ParseException {

        int opcio = menu();
        while (opcio != 0) {
            switch (opcio) {
                case 0:
                    opcio = 0;
                    break;
                case 1:
                    afegirClient();
                    break;
                case 2:
                    afegirComandaClient();
                    break;
                case 3:
                    consultarClientFacturacio();
                    break;
                case 4:
                    consultarClientComandes();
                    break;
            }
        }

        System.out.println("Gracies per utilitzar el nostre programa, fins la proxima :)");
    }
}

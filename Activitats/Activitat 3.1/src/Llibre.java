import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Llibre {
    String autor;
    String titol;
    
    public Llibre() {
        
    }
    
    public Llibre(String autor, String titol) {
        this.autor = autor;
        this.titol = titol;
    }
   
    public void imprimir() {
        System.out.println("Títol: "+this.titol);
        System.out.println("Autor: "+this.autor);
        System.out.println("---");
    }
    
    public void desar_xml (Element nodeLlibres, Document doc) throws ParserConfigurationException {
        Element nodeLlibre = doc.createElement("llibre");
        nodeLlibre.setAttribute("autor", this.autor);
        nodeLlibre.setAttribute("titol", this.titol);
        nodeLlibres.appendChild(nodeLlibre);
    }
    
    public void llegir (Node nodeLlibre) {
        NamedNodeMap atributs = nodeLlibre.getAttributes();
        this.titol = atributs.getNamedItem("titol").getTextContent();
        this.autor = atributs.getNamedItem("autor").getTextContent();
    }
}


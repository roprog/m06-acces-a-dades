import java.io.File;

public class acti {

    static String path1 = "E:\\Coses\\Edt II\\M06 - Acces a dades\\Proves\\Package1";
    static String path2 = "E:\\Coses\\Edt II\\M06 - Acces a dades\\Proves\\Package2";

    static File dir1 = new File(path1);
    static File dir2 = new File(path2);

    public static void main(String[] args) {

        System.out.println("---- Directori A: " + path1);
        System.out.println("---- Directori B: " + path2);

        for (int i = 0; i < dir1.list().length; i++) {
            
            int enter = 0;
            
            for (int j = 0; j < dir2.list().length; j++) {
                
                // Comparation between both of the directorys
                if (dir1.list()[i].equals(dir2.list()[j])) {

                    System.out.println("- El fitxer " + dir1.list()[i] + " existeix a ambdos directoris");

                    // Comparation between the dates of the files
                    if (dir1.listFiles()[i].lastModified() == dir2.listFiles()[j].lastModified()) {

                        System.out.println("---- Tenen la mateixa data");

                    } else if (dir1.listFiles()[i].lastModified() > dir2.listFiles()[j].lastModified()) {

                        System.out.println("---- Al directori A es mes nou que al directori B");
                    } else {

                        System.out.println("---- Al directori B es mes nou que al directori A");
                    }
                    // Comparation between the size of the files
                    if (dir1.listFiles()[i].length() == dir2.listFiles()[j].length()) {

                        System.out.println("---- Tenen la mateixa mida");
                        
                    } else if (dir1.listFiles()[i].length() > dir2.listFiles()[j].length()) {

                        System.out.println("---- Al directori A es mes gran que al directori B");
                    } else {

                        System.out.println("---- Al directori B es mes gran que al directori A");
                    }
                    enter = 1;
                } 
            }
            if (enter == 0) {

                System.out.println("- El fitxer " + dir1.list()[i] + " existeix al directori A, pero no al B");

                enter = 0;
            }
        }
    }
}
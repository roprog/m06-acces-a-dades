import java.io.File;

public class main {

 public static void main(String[] args) {

  String ruta = "/Users/Jordi/Documents/kk";
  esborra_contingut(ruta);
 }

 public static void esborra_fitxer(File f) {
  if (f.delete()) 
   System.out.println("S'ha esborrat el fitxer " + f.getName()) ;
  else
   System.out.println("Error a l'esborrar el fitxer " + f.getName()) ;
 }

 public static void esborra_contingut(String ruta) {
  File dir = new File(ruta);
  for (File f : dir.listFiles())
   esborra_fitxer(f);
  esborra_fitxer(dir);
 }
}
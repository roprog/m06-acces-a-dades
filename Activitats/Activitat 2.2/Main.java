import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ac2_2 {
    
    static String ruta = "/Users/Jordi/ac2-2.txt";

    public static int menu() {
        System.out.println("Seleccioneu una opció:"
                + "\n 0.- Sortir"
                + "\n 1.- Escriure enters"
                + "\n 2.- Llegir enters");
        
        Scanner teclat = new Scanner(System.in);      
        return teclat.nextInt(); // TODO comprovar que el número és correcte
    }
    
    public static List<Integer> demanarEnters() {
        Scanner teclat = new Scanner(System.in);      
        System.out.println("Quants enters voleu escriure?");
        int numEnters = teclat.nextInt(); // TODO comprovar que és un número vàlid
        List<Integer> llistaEnters = new ArrayList<>(); // es podria fer servir int[]
        for (int i=0; i < numEnters; i++) {
            System.out.println("Introduiu el següent enter: ");
            llistaEnters.add(teclat.nextInt());
        }
        return llistaEnters;
    }
    
    public static void desarEnters(List<Integer> llistaEnters) throws FileNotFoundException, IOException {
        DataOutputStream streamEscriptura = new DataOutputStream(new
        BufferedOutputStream(new FileOutputStream(ruta)));

        try {
            streamEscriptura.writeInt(llistaEnters.size()); // desem quants enters hi ha
            for (int i=0; i < llistaEnters.size(); i++) {
                streamEscriptura.writeInt(llistaEnters.get(i)); // desem cada enter
            }
        }
        finally {
            streamEscriptura.close();
        }
    }
    
    public static List<Integer> llegirEnters() throws FileNotFoundException, IOException {
        DataInputStream streamLectura = new DataInputStream(new
        BufferedInputStream(new FileInputStream(ruta)));
        List<Integer> llistaEnters = new ArrayList<>(); // es podria fer servir int[]

        try {
            int numEnters = streamLectura.readInt(); // recuperem quants enters hi ha
            for (int i=0; i < numEnters; i++) {
                llistaEnters.add(streamLectura.readInt()); // desem cada enter
            }
        }
        finally {
            streamLectura.close();
        }
        
        return llistaEnters;
    }
    
    public static void imprimirLlista(List<Integer> llistaEnters) {
        System.out.println("======================="); 
        for (int i=0; i < llistaEnters.size(); i++) {
             System.out.println(llistaEnters.get(i));
         }
        System.out.println("======================="); 

    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO: gestionar l'excepció de fitxer no trobat o IOException
        int opcio = menu();
        List<Integer> llistaEnters;
        switch (opcio) {
            case 1:
                llistaEnters = demanarEnters();
                desarEnters(llistaEnters);
                break;
            case 2:
                llistaEnters = llegirEnters();
                imprimirLlista(llistaEnters);
                break;
       
        }
    }
    
}

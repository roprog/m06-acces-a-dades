import java.io.*;
import java.util.Properties;

public class Fichero {

    public static Habitacio leerDeDisco(String ruta) {
        Habitacio hab = null;
        try (DataInputStream lectura = new DataInputStream(new BufferedInputStream(new FileInputStream(ruta)))) {
            hab = new Habitacio(lectura.readDouble(), lectura.readDouble(), lectura.readUTF());
            int vueltas = lectura.readInt();
            for (int i = 0; i < vueltas; i++) {
                Moble moble = new Moble(lectura.readDouble(), lectura.readDouble(), lectura.readInt());
                hab.getMobles().add(moble);
            }
        } catch (IOException e) {
            System.out.println("Error al leer el fichero.");
        }

        return hab;
    }

    public static void guardarEnDisco(Habitacio hab, String ruta) {
        try (DataOutputStream escritura = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(ruta)))) {
            escritura.writeDouble(hab.getAmple());
            escritura.writeDouble(hab.getLlarg());
            escritura.writeUTF(hab.getNom());
            escritura.writeInt(hab.getMobles().size());
            for (Moble moble : hab.getMobles()) {
                escritura.writeDouble(moble.getAmple());
                escritura.writeDouble(moble.getLlarg());
                escritura.writeInt(moble.getColor());
            }
        } catch (IOException e) {
            System.out.println("Error al guardar en el disco");
        }
    }

    public static void crearProp(File fileProp) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileProp))) {
            writer.write("AMPLE=10.5");
            writer.write("\nLLARG=5.5");
            writer.write("\nNOM=Salon");
        } catch (Exception e) {
            System.out.println("Error al crear el fichero properties");
        }
    }

    public static Habitacio crearHab(File fileProp) {
        Properties properties = new Properties();

        try {
            //Creamos el fichero properties solo en caso de que no exista
            if (fileProp.createNewFile()) {
                //Almacenamos los datos en el fichero properties después de crearlo
                crearProp(fileProp);
            }
            properties.load(new FileInputStream(fileProp));

        } catch (Exception e) {
            System.out.println("Error al crear la habitación");
        }

        double ample = Double.parseDouble(properties.getProperty("AMPLE"));
        double llarg = Double.parseDouble(properties.getProperty("LLARG"));
        String nom = properties.getProperty("NOM");
        return new Habitacio(ample, llarg, nom);
    }

    public static Habitacio asignarHab(String rutaProp, String rutaHab) {
        File fileHab = new File(rutaHab);
        File fileProp = new File(rutaProp);
        Habitacio habitacio;
        if (fileHab.exists()) {
            //Si tenemos un fichero habitació, lo leemos y asignamos a la habitación.
            habitacio = leerDeDisco(rutaHab);
        } else {
            //Si no lo tenemos, creamos una habitación con los datos de properties.
            habitacio = crearHab(fileProp);
            //La guardamos en el disco.
            guardarEnDisco(habitacio, rutaHab);
        }

        return habitacio;
    }

    public static void main(String[] args) {
        Habitacio hab = new Habitacio(1,1,"has");
        String ruta = "";
        guardarEnDisco(hab, ruta);
    }
}

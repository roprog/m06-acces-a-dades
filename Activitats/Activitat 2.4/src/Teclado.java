import java.util.InputMismatchException;
import java.util.Scanner;

public class Teclado {
    private static Scanner sc = new Scanner(System.in);
    private static boolean error;

    //Método para leer un entero filtrando errores
    public static int leerInt() {
        int valor = 0;
        do {
            try {
                valor = sc.nextInt();
                error = false;
            } catch (InputMismatchException e) {
                System.out.println("Has ingresado un valor incorrecto. Ingresa un número entero.");
                error = true;
                sc.nextLine();
            }
        } while (error);
        return valor;
    }

    //Método para leer un entero entre un rango de números
    public static int leerInt(int min, int max) {
        int valor;
        do {
            valor = leerInt();
            if (valor < min || valor > max) {
                System.out.println("Opción escogida no válida.");
                System.out.println("Ingrese un valor entre " + min + " y " + max);
                error = true;
            } else {
                error = false;
            }
        } while (error);
        return valor;
    }

    //Método para leer un double filtrando errores
    public static double leerDouble() {
        double valor = 0;
        do {
            try {
                valor = sc.nextDouble();
                error = false;
            } catch (InputMismatchException e) {
                System.out.println("Has ingresado un valor incorrecto. Ingresa un número.");
                error = true;
                sc.nextLine();
            }
        } while (error);
        return valor;
    }
}

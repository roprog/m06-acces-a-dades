public class Main {
    public static int menu() {
        System.out.println("\nElija una opción");
        System.out.println("" +
                "0. Salir\n" +
                "1. Mostrar por pantalla los datos de la habitación\n" +
                "2. Añadir un mueble a la habitación\n" +
                "3. Guardar la habitación en el disco\n" +
                "4. Listar por pantalla todos los muebles de la habitación, con todos sus datos:");
        return Teclado.leerInt(0, 4);
    }

    public static void main(String[] args) {
        //Ruta del fichero properties
        String rutaProp = "src\\activitat2_4\\hab.properties";
        //Ruta del fichero habitacio.txt
        String rutaHab = "C:\\Users\\bwog_\\Documents\\Actividades Clase\\M06\\Habitacio.txt";

        Habitacio habitacio = Fichero.asignarHab(rutaProp, rutaHab);
        boolean continuar = true;

        while (continuar) {
            switch (menu()) {
                case 0:
                    System.out.println("El programa ha finalizado.");
                    continuar = false;
                    break;
                case 1:
                    habitacio.mostrarDatos();
                    break;
                case 2:
                    //Agrega muebles a la habitación.
                    habitacio.agregarMuebles();
                    break;
                case 3:
                    //Guarda los datos de la habitación en el disco.
                    Fichero.guardarEnDisco(habitacio, rutaHab   );
                    break;
                case 4:
                    //Muestra los datos de la habitación guardada en el disco.
                    //No mostrará los nuevos muebles hasta que se hayan guardado.
                    Habitacio habLectura = Fichero.leerDeDisco(rutaHab);
                    if (habLectura == null){
                        System.out.println("Se ha eliminado el fichero en medio de la ejecución.\n" +
                                "Seleccione la opción 3 para volver a guardarlo.");
                    }else {
                        habLectura.mostrarMuebles();
                    }
                    break;
            }
        }
    }
}




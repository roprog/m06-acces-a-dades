import java.io.DataOutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.DataInputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;


public class ex1 {

	public static void write() {
		
		String ruta = "/home/users/inf/wiam2/iam26296830/Iam/M06 - Acces a Dades/DIRTEST";

	    DataOutputStream streamEscriptura = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(ruta)));

	    streamEscriptura.writeBoolean(true);
	    streamEscriptura.writeInt(100001);	    
		streamEscriptura.writeChars("hola");
	    
		streamEscriptura.close();

	}
	
	public static void read() {
		
		String ruta = "/home/users/inf/wiam2/iam26296830/Iam/M06 - Acces a Dades/DIRTEST";
		
		DataInputStream streamLectura = new DataInputStream(new BufferedInputStream(new FileInputStream(ruta)));
		
		boolean valor1 = streamLectura.readBoolean();
		int valor2 = streamLectura.readInt();
		char valor3 = streamLectura.readChar();
		
		streamLectura.close();
	
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ex1.write();
		ex1.read();
		
		
	}

}

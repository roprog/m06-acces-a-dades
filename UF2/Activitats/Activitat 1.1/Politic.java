import java.sql.Date;

public class Politic {

	private String nif;
	private String nom;
	private Date dataNaixement;
	private int sou;
	private boolean esCorrupte;
	
	public Politic(String nif, String nom, Date dataNaixement, int sou, boolean esCorrupte) {
		this.nif = nif;
		this.nom = nom;
		this.dataNaixement = dataNaixement;
		this.sou = sou;
		this.esCorrupte = esCorrupte;
	}

	@Override
	public String toString() {
		return "Politic [nif=" + nif + ", nom=" + nom + ", dataNaixement=" + dataNaixement + ", sou=" + sou
				+ ", esCorrupte=" + esCorrupte + "]";
	}
	
	
}

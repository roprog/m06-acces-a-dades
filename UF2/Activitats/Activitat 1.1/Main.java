import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Main {
	
	public static void createTable(Connection con) throws SQLException {
		
		String sentenciaSQL = "CREATE TABLE POLITIC ("
				+ "nif VARCHAR(50)," 
				+ "nom VARCHAR(50)," 
				+ "dataNaixement DATE(50)," 
				+ "sou INTEGER," 
				+ "esCorrupte CHAR(50)," 
				+ ");";
		
		Statement statement = con.createStatement();
		statement.execute(sentenciaSQL);
	}

	public static Politic createPolitic() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix el nif: \n");
		String nif = sc.next();
		System.out.println("Introdueix el nom: \n");
		String nom = sc.next();
		System.out.println("Introdueix la data de naixement: \n");
		Date dataNaixement = Date.valueOf(sc.nextLine());
		System.out.println("Introdueix el sou: \n");
		int sou = sc.nextInt();
		System.out.println("És corrupte? \n");
		boolean esCorrupte = sc.nextBoolean();
		
		Politic politic = new Politic(nif,nom,dataNaixement,sou,esCorrupte);

		sc.close();
		return politic;
	}
	
	public static void addPolitic(Connection con,String nif, String nom, Date dataNaixement, int sou, boolean esCorrupte) throws SQLException {
		
		String sentenciaSQL = "insert into politics (nif) values ('" + nif + "')";
		Statement statement = con.createStatement();
		statement.execute(sentenciaSQL);
		sentenciaSQL = "insert into politics (nom) values ('" + nom + "')";
		statement = con.createStatement();
		statement.execute(sentenciaSQL);
		sentenciaSQL = "insert into politics (dataNaixement) values ('" + dataNaixement + "')";
		statement = con.createStatement();
		statement.execute(sentenciaSQL);
		sentenciaSQL = "insert into politics (sou) values ('" + sou + "')";
		statement = con.createStatement();
		statement.execute(sentenciaSQL);
		sentenciaSQL = "insert into politics (esCorrupte) values ('" + esCorrupte + "')";
		statement = con.createStatement();
		statement.execute(sentenciaSQL);
	}
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		
		String url="jdbc:mysql://localhost/first";
		String usuari="rogerv";
		String contrasenya="170520011";
		Connection con = DriverManager.getConnection(url, usuari, contrasenya);
		
		//Crea la taula POLITIC
		createTable(con);
		//Crea un objecte de la classe Politic
		createPolitic();
		//Afageix un Politic a la taula Politics
		addPolitic(con,"26294631T", "Kanye West", Date.valueOf("1987-05-17"), 15000, false);
		
	}

}
	
	public static void addPolitic(String nif, String nom, Date dataNaixement, int sou, boolean esCorrupte) {
		
		
	}
	
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		
		String url="jdbc:mysql://localhost/first";
		String usuari="rogerv";
		String contrasenya="170520011";
		Connection con = DriverManager.getConnection(url, usuari, contrasenya);
		
		//Crea la taula POLITIC
		createTable(con);
		//Crea un objecte de la classe Politic
		createPolitic();
		//Afageix un Politic a la taula Politics
		addPolitic("26294631T", "Kanye West", Date.valueOf("1987-05-17"), 15000, false);
		
	}

}
DROP TABLE IF EXISTS RESUM_FACTURACIO;
CREATE TABLE RESUM_FACTURACIO (mes int, any int, dni_client int(8), quantitat decimal(10, 2),
                                CONSTRAINT PK_RESUM_FACTURACIO PRIMARY KEY (mes, any, dni_client), 
                                FOREIGN KEY (dni_client) REFERENCES client(DNI));

DROP PROCEDURE IF EXISTS crea_resum_facturacio;
DELIMITER //
CREATE PROCEDURE crea_resum_facturacio(IN p_mes int, p_any int)
BEGIN
	DECLARE vDni int(8);
    DECLARE vQuantitat decimal(10, 2);
    DECLARE vTotal decimal(20, 2);
    DECLARE acaba INT DEFAULT FALSE; 
	DECLARE clients CURSOR FOR SELECT DISTINCT DNI_client FROM comanda WHERE MONTH(Data) = p_mes AND YEAR(Data) = p_any;
    DECLARE comandes CURSOR FOR SELECT Preu_total FROM comanda WHERE DNI_client = vDni AND MONTH(Data) = p_mes AND YEAR(Data) = p_any;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET acaba = TRUE; 

    OPEN clients;
    loop1: LOOP
		FETCH clients INTO vDni;
        IF acaba THEN
			LEAVE  loop1;
		END IF;
        
        SET vTotal = 0;
        OPEN comandes;
        loop2: LOOP
			FETCH comandes into vQuantitat;
			IF acaba THEN
				LEAVE loop2;
			END IF;
            
            SET vTotal = vTotal + vQuantitat;
        END LOOP;
        close comandes;
        
        INSERT INTO RESUM_FACTURACIO values (p_mes, p_any, vDni,vTotal);
        SET acaba = FALSE;
    END LOOP;
	close clients;
END;

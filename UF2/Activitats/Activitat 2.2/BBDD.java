import java.sql.Statement;
import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BBDD {

    Connection con;

    public BBDD() throws SQLException {
        String url="jdbc:mysql://localhost/bbdd"; 
        String usuari="db_user";
        String password="contrasenya";
        this.con = DriverManager.getConnection(url, usuari, password);       
    }

    public void creaTaulaClient() throws SQLException{

        String sentenciaSQL;
        Statement statement;
        sentenciaSQL = "CREATE TABLE client ("
            + "dni INT(10) PRIMARY KEY,"
            + "nom VARCHAR,"
            + "premium ENUM('S', 'N')"
            + ")";
        statement = con.createStatement();
        statement.execute(sentenciaSQL);
        System.out.println("Taula creada correctament.");
    }
    
    public void creaTaulaComanda() throws SQLException{

        String sentenciaSQL;
        Statement statement;
        sentenciaSQL = "CREATE TABLE comanda ("
            + "num_comanda INT PRIMARY KEY,"
            + "preu_total FLOAT,"
            + "data DATE,"
            + "dni_client INT(10) FOREIGN KEY"
            + ")";
        statement = con.createStatement();
        statement.execute(sentenciaSQL);
        System.out.println("Taula creada correctament.");
    }

    public void crearTaulaComandaIClient() throws SQLException {
        creaTaulaClient();
        creaTaulaComanda();
    }

    public void generarResumFacturacio() throws SQLException {

        Scanner sc = new Scanner(System.in);

        System.out.println("Has seleccionat: Generació de resum de facturació");
        System.out.println("Introdueix el mes (FORMAT 3 || 12):");
        int mes = sc.nextInt();
        if (mes < 1 || mes > 12) {
            System.out.println("No has introduit un valor valid, introdueix-lo amb el seguent format: 3 || 11");
            mes = sc.nextInt();
        }
        System.out.println("Introdueix l'any (FORMAT del 2019 fins el 9999):");
        int any = sc.nextInt();
        if (any < 2019 || any > 9999) {
            System.out.println("No has introduit un valor valid, introdueix-lo amb el seguent format: del 2019 fins el 9999");
            any = sc.nextInt();
        }

        //a partir d'aqui mhan ajudat, no sabia ben be com fer-ho
        String sql = "CALL crea_resum_facturacio(?,?)";
        PreparedStatement pStatement = null;
        Connection connection;

        connection.setAutoCommit(false);
        pStatement = connection.prepareStatement(sql);
        pStatement.setInt(1, mes);
        pStatement.setInt(2, any);
        pStatement.executeUpdate();

        connection.commit();

    }

    /*public void recuperarDadesBBDD() throws SQLException {

        String sentenciaSQL;
        String sentenciaSQLComanda;
        Statement statement = con.createStatement();        

        sentenciaSQL = "select * from client;";
        ResultSet rs = statement.executeQuery(sentenciaSQL);

        sentenciaSQLComanda = "select * from comanda;";
        ResultSet rsComanda = statement.executeQuery(sentenciaSQLComanda);

        for (Client client : Main.clients) {

            client.dni = rs.getInt("dni");
            client.nom = rs.getString("nom");

            String clientPremium = rs.getString("premium");
            String comparator = "S";

            if (clientPremium.equals(comparator)) {
                client.premium = true;
            } else {
                client.premium = false;
            }

            for (Comanda comanda : client.comandes) {
                
                comanda.num_comanda = rsComanda.getInt("num_comanda");
                comanda.preu_total = rsComanda.getFloat("preu_total");
                //comanda.data = rsComanda.getDate("data");
            }
        }
    }

    public void esborrarBBDD() throws SQLException {

        Statement statement = con.createStatement();  

        String sentenciaSQL = "TRUNCATE TABLE client;";
        statement.executeQuery(sentenciaSQL);

        sentenciaSQL = "TRUNCATE TABLE comanda;";
        statement.executeQuery(sentenciaSQL);
    }

    public void guardadDadesBBDD() throws SQLException {

        String sentenciaSQL;
        Statement statement = con.createStatement(); 
        
        for (Client clients : Main.clients) {
            
            sentenciaSQL = "insert into clients (dni) values ('" + clients.dni + "');";
            statement.executeUpdate(sentenciaSQL);

            sentenciaSQL = "insert into clients (nom) values ('" + clients.nom + "');";
            statement.executeUpdate(sentenciaSQL);

            if (clients.premium == true) {
                sentenciaSQL = "insert into clients (premium) values ('S')";
                statement.executeUpdate(sentenciaSQL);
            } else {
                sentenciaSQL = "insert into clients (premium) values ('N')";
                statement.executeUpdate(sentenciaSQL);
            }

            for (Comanda comanda : clients.comandes) {
                
                sentenciaSQL = "insert into clients (num_comanda) values ('" + comanda.num_comanda + "');";
                statement.executeUpdate(sentenciaSQL);

                sentenciaSQL = "insert into clients (preu_total) values ('" + comanda.preu_total + "');";
                statement.executeUpdate(sentenciaSQL);

                sentenciaSQL = "insert into clients (data) values ('" + comanda.data + "');";
                statement.executeUpdate(sentenciaSQL);

                sentenciaSQL = "insert into clients (dni_client) values ('" + clients.dni + "');";
                statement.executeUpdate(sentenciaSQL);

            }
        }
    }

    public void crearNouCLient() {

        Scanner sc = new Scanner(System.in);
        
        System.out.println("Introdueix les dades del client que vols afegir: ");
        System.out.println("dni sense la lletra (format: 12345678): ");
        int dni = sc.nextInt();

        System.out.println("Nom del client: ");
        String nom = sc.nextLine();

        System.out.println("Es premium? (format: S | N; S = true i N = false)");
        String stringPremium = sc.next();

        String comparator = "S";
        boolean premium;

        if (stringPremium.equals(comparator)) {
            premium = true;
        } else {
            premium = false;
        }

        Main.clients.add(new Client(dni, nom, premium));

        sc.close();
    }

    public int printClientDades() {

        Scanner sc = new Scanner(System.in);
        int contador = 1;

        System.out.println("Aquests son els clients:");

        for (Client client : Main.clients) {
            
            System.out.println(contador + ") " + client.dni + " - " + client.nom);
            contador++;
        }

        System.out.println("Seleccioneu el client de la comanda:");
        int numClient = sc.nextInt();

        sc.close();
        return numClient;

    }

    public void crearNovaComanda(int numClient) {

        Scanner sc = new Scanner(System.in);
        Client client = new Client();

        System.out.println("Introdueix les dades de la comanda que vols afegir: ");
        System.out.println("Numero de la comanda (format: 1): ");
        int num_comanda = sc.nextInt();

        System.out.println("Preu total de la comanda amb dos decimals (format: 46,23): ");
        float preu_total = sc.nextFloat();

        //System.out.println("Es premium? (format: S | N; S = true i N = false)");
        //String data = sc.next();

        for (int i = 1; i < Main.clients.size() + 1; i++) {
            if (i == numClient) {
                client.comandes.add(new Comanda(num_comanda, preu_total));
                break;
            }
        }
    }

    public void printComandes(int numClient) {

        Client client = new Client();

        for (int i = 1; i < Main.clients.size() + 1; i++) {
            if (i == numClient) {

                for (Comanda comanda : client.comandes) {      
                    comanda.toString();
                }
                break;
            }
        }
    }*/

    
}

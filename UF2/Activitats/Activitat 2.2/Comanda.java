import java.time.LocalDate;

public class Comanda {
    int num_comanda;
    float preu_total;
    LocalDate data;

    public Comanda() {}

    public Comanda(int num_comanda, float preu_total) {
        this.num_comanda = num_comanda;
        this.preu_total = preu_total;
    }

    @Override
    public String toString() {
        return "Comanda [num_comanda=" + num_comanda + ", preu_total=" + preu_total + ", data=" + data + "]";
    }
}

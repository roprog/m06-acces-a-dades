public class Bbdd {
    
    //Se selecciona el client i s'elimina el client i totes les seves comandes de la BBDD
    public static void eliminarClient() throws Exception{

        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();

        Scanner sc = new Scanner(System.in);

        try {
            
            System.out.println("Entra el id del Client que vols eliminar: ");
            int id = sc.nextInt();

            session.beginTransaction();

            Client c = (Client) session.get(Client.class, id);

            for (Comanda comanda : c.comandes) {
                int numComanda = comanda.getNum_comanda();
                Comanda com = (Comanda) session.get(Comanda.class, numComanda);
                session.delete(com);
            }

            session.delete(c);

            session.getTransaction().commit();

            //Fen't-ho diferent
            /*String hql = "delete from Client where id = '" + id + "'";
            Query query = session.createQuery(hql);*/

            System.out.println("S'ha eliminat el Client amb id: " + id);

        } catch (Exception e) {
            System.out.println("Algo ha fallat, comprova que estiguis eliminant un Client existent");

        } finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }

    /*Se selecciona un client, i es tornen a introduir les seves dades 
    (les que no siguin PK, és a dir, nom i premium)*/
    public static void modificarClient() throws Exception{

        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();

        Scanner sc = new Scanner(System.in);

        try {
            
            System.out.println("Les dades de quin client vols modificar (id)? ");
            int id = sc.nextInt();
            System.out.println("Quin nou nom li vols donar? ");
            String nom = sc.nextLine();
            System.out.println("Es premium (S = Si, N = No)? ");
            String premium = sc.next();

            session.beginTransaction();

            Client c = (Client) session.get(Client.class, id);

            c.setNom(nom);
            if (premium.equals("S")) {
                c.setPremium(true);
            } else {
                c.setPremium(false);
            }

            session.update(c);
            session.getTransaction().commit();

        } catch (Exception e) {
            System.out.println("Algo ha fallat, comprova que estiguis modicicant un Client existent, i que hagis introduit els parametres correctament, segons el exemple: ");
            System.out.println("Nom = Nil\nPremium = S");

        } finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }

    //Mostrar per pantalla els clients de la BBDD que el seu nom comenci pel text introduït per teclat
    public static void cercarNomClient() throws Exception{

        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();

        String text = null;

        Scanner sc = new Scanner(System.in);

        try {
            
            System.out.println("Quin Client vols buscar? (tingues en compte les majuscules)");
            text = sc.next();

            session.beginTransaction();

            Query query = session.createQuery("from Client where nom like '" + text + "%'");
            List<Client> listClients = query.list();

            for (Client client : listClients) {
                System.out.println(client.toString());
            }

        } catch (Exception e) {
            System.out.println("Algo ha fallat, comprova que estiguis buscant un Client que comenci amb: " + text);

        } finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }
    
}

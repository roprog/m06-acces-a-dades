public class Main {

    public static List<Client> clients = new ArrayList<>();

    //Dona d'alta un nou Client
    public static void nouClient() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Introdueix les dades del nou CLient que vols afegir: ");
        System.out.println("Introduexi els numeros del dni (12345678): ");
        int id = sc.nextInt();

        System.out.println("Introduexi el num del client (Nil): ");
        String nom = sc.next();

        System.out.println("Digues si es un client premium (Si o No): ");
        String premiumStr = sc.next();
        boolean premium;
        if (premiumStr.equals("Si")) {
            premium = true;
        } else {
            premium = false;
        }

        Client cl1 = new Client(id, nom, premium);
        clients.add(cl1);
    }

    //Dona d'alta una nova Comanda d'un CLient
    public static void novaComanda() throws ParseException, Exception{

        try {
            Scanner sc = new Scanner(System.in);
            int contador = 1;

            System.out.println("Aquests són els clients: ");

            //Pausa del programa per llegir el print
            try { 
                Thread.sleep(2000); 
            } catch (InterruptedException e ) { 
                System.out.println("Thread Interrupted") ;
            }

            for (Client client : clients) {
                System.out.println(contador + ") " + client.toString());
                contador++;
            }

            System.out.println("Seleccioneu el client de la comanda: ");
            int selection = sc.nextInt();

            System.out.println("Introdueix les dades de la comanda que vols afegir:");
            System.out.println("Numero de comanda (1): ");
            int num_comanda = sc.nextInt();

            System.out.println("Preu de la comanda (dos decimals = 35,65): ");
            float preu_total = sc.nextFloat();

            System.out.println("Data de la comanda (07/01/2021): ");
            String data = sc.next();

            Date dateFormatted = new SimpleDateFormat("dd/MM/yyyy").parse(data);

            contador = 1;
            for (Client client : clients) {
                if (contador == selection) {
                    client.comandes.add(new Comanda(num_comanda, preu_total, dateFormatted));
                }
                contador++;
            }
        } catch (Exception e) {
            System.out.println("S'ha produit un error, assegure't de que has seleccionat be el parametres");
        }
    }

    //
    public static void mostrarComandes() throws Exception{

        try {
            Scanner sc = new Scanner(System.in);
            int contador = 1;

            System.out.println("Aquests són els clients: ");

            //Pausa del programa per llegir el print
            try { 
                Thread.sleep(2000); 
            } catch (InterruptedException e ) { 
                System.out.println("Thread Interrupted") ;
            }

            for (Client client : clients) {
                System.out.println(contador + ") " + client.toString());
                contador++;
            }

            System.out.println("Seleccioneu el client de la comanda: ");
            int selection = sc.nextInt();

            for (Client client : clients) {
                if (contador == selection) {
                    for (Comanda comanda : client.comandes) {
                        System.out.println(comanda.toString());
                    }
                }
                contador++;
            }

        } catch (Exception e) {
            
        }
    }

    // Mostrar per pantalla un resum de tots els clients amb la quantitat total que han facturat, ordenat de més facturació a menys facturació
    public static void resumClients() throws Exception{

        try {
            float totalFacturat = 0;

            for (Client client : clients) {
                for (Comanda comanda : client.comandes) {
                    totalFacturat = totalFacturat + comanda.getPreu_total();
                }
                //No he aconseguit fer que surtin per orde descendent
                System.out.println(client.getNom() + " " + totalFacturat);
                totalFacturat = 0;
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static int menu() {

        Scanner sc = new Scanner(System.in);

        System.out.println("\nEscull una opcio");
        System.out.println("" +
                "0. Eliminar un client\n" +
                "1. Actualitzar les dades d'un client\n" +
                "2. Cercar per nom un client\n" +
                "3. Donar d'alta un nou client\n" +
                "4. Donar d'alta una nova comada\n" +
                "5. Mostrar per pantalla les comandes d'un client\n" +
                "6. Mostrar per pantalla un resum de tots els clients");

        return sc.nextInt();
    }


    public static void main(String[] args) throws Exception {

        switch (menu()) {
            case 0:
                Bbdd.eliminarClient();
                break;
            case 1:
                Bbdd.modificarClient();
                break;
            case 2:
                Bbdd.cercarNomClient();
                break;
            case 3:
                nouClient();
                break;
            case 4:
                novaComanda();
                break;
            case 5:
                mostrarComandes();
                break;
            case 6:
                resumClients();
                break;
            default:
                break;
        }
    }

}

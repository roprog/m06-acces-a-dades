public class Comanda {

    private int num_comanda;
    private float preu_total;
    private Date data;
    
    public Comanda(){}

    public Comanda(int num_comanda, float preu_total, Date data) {
        this.num_comanda = num_comanda;
        this.preu_total = preu_total;
        this.data = data;
    }

    @Override
    public String toString() {
        return "Comanda [data=" + data + ", num_comanda=" + num_comanda + ", preu_total=" + preu_total + "]";
    }

    public int getNum_comanda() {
        return num_comanda;
    }

    public void setNum_comanda(int num_comanda) {
        this.num_comanda = num_comanda;
    }

    public float getPreu_total() {
        return preu_total;
    }

    public void setPreu_total(float preu_total) {
        this.preu_total = preu_total;
    }

    
}

public class Client {
    
    private int dni;
    private String nom;
    private boolean premium;
    public List<Comanda> comandes = new ArrayList<>();

    public Client() {}

    public Client(int dni, String nom, boolean premium) {
        this.dni = dni;
        this.nom = nom;
        this.premium = premium;
    }

    public Client(int dni, String nom, boolean premium, List<Comanda> comandes) {
        this.dni = dni;
        this.nom = nom;
        this.premium = premium;
        this.comandes = comandes;
    }

    

    @Override
    public String toString() {
        return "\t" + dni + " - " + nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }
}

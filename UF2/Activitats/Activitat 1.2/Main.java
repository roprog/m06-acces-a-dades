import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    static List<Client> clients = new ArrayList<>();

    public static int menu() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Escull l'opció que vols executar:" 
            + "\n 1.- Creacio del model"
            + "\n 2.- Recuperacio de les dades de la BBDD"
            + "\n 3.- Emmagatzemat de dades a la BBDD"
            + "\n 4.- Alta d'un nou client"
            + "\n 5.- Alta d'una nova comanda"
            + "\n 6.- Mostrar per pantalla les comandes d'un client");

        int result = sc.nextInt();
        sc.close();

        return result;
    }
    public static void main(String[] args) throws Exception {
        
        BBDD bbdd = new BBDD();
        int numClient;

        switch (menu()) {
            case 1:
                //Crea el model (les dues taules) dins la BBDD. 
                //Mostra un error si les taules ja existien.
                bbdd.crearTaulaComandaIClient();
                break;
            case 2:
                //Recupera totes les dades de la BBDD
                //Les guarda en una la llista de clients (amb les seves comandes).
                bbdd.recuperarDadesBBDD();
                break;
            case 3:
                //Elimina totes les dades de la BBDD
                bbdd.esborrarBBDD();
                //Guarda totes les dades que hi ha a la llista de clients
                bbdd.guardadDadesBBDD();
                break;
            case 4:
                //Demana les dades del client (dni, nom i si és "premium" o no)
                //L'afegeix a la llista de clients
                bbdd.crearNouCLient();
                break;
            case 5:
                //Demana en primer lloc les dades del client
                numClient = bbdd.printClientDades();
                //Després es demanen les dades bàsiques de la comanda (número, preu i data)
                bbdd.crearNovaComanda(numClient);
                break;
            case 6:
                //Demana en primer lloc les dades del client
                //Imprimeix les comandes del client
                numClient = bbdd.printClientDades();
                bbdd.printComandes(numClient);
                break;
        }
    }
}
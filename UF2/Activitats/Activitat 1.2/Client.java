import java.util.ArrayList;
import java.util.List;

public class Client {
    int dni;
    String nom;
    boolean premium;
    List<Comanda> comandes = new ArrayList<>();

    public Client() {}

    public Client(int dni, String nom, boolean premium) {
        this.dni = dni;
        this.nom = nom;
        this.premium = premium;
    }

    public void addComanda() {

        Comanda e = new Comanda();
        comandes.add(e);

    } 
}